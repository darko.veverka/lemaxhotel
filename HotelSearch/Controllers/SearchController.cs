﻿using HotelSearch.Models.Search;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace HotelSearch.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class SearchController : Controller
    {
        [HttpGet]
        [Produces("application/json")]
        [SwaggerOperation(Summary = "Returns all hotels")]
        public async Task<ActionResult<IList<HotelSearchResult>>> GeoSearch()
        {
            var list = new List<HotelSearchResult>();
            return Ok(list);
        }
    }
}
