﻿using HotelSearch.Interface;
using HotelSearch.Models.Dto;
using Microsoft.Extensions.Caching.Memory;
using System.Data.Entity.Core;

namespace HotelSearch.Repository
{
    public class Repository : IRepository
    {
        private readonly ILogger<Repository> _logger;
        private readonly IMemoryCache _cache;

        public Repository(ILogger<Repository> logger, IMemoryCache cache)
        {
            _logger = logger;
            _cache = cache;
        }
        public async Task<bool> DeleteAsync(string id, CancellationToken cancellationToken = default)
        {
            try
            {
                await Task.Run(() => _cache.Remove(id), cancellationToken);
                return true;
            }
            catch (Exception ex) {
                _logger.LogError(ex.Message);
                return false;
            }
        }

        public async Task<List<Hotel>> GetAllAsync(CancellationToken cancellationToken = default)
        {
            var result = await Task.Run(() => _cache.Get<List<Hotel>>("hotels"), cancellationToken);
            return result;
        }

        public async Task<Hotel> GetAsync(string id, CancellationToken cancellationToken = default)
        {
            var result = await GetAllAsync();
            return result.FirstOrDefault(x => x.Id == id);
        }

        public async Task<bool> SaveAsync(Hotel model, CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await GetAllAsync();
                result.Add(model);
                _cache.Set("hotels", result);
                return true;
            }
            catch (Exception ex) 
            {
                _logger.LogError(ex.Message);
                return false;
            }
        }

        public async Task<bool> UpdateAsync(Hotel model, CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await GetAllAsync();
                var hotel = result.FirstOrDefault(x => x.Id == model.Id);
                if (hotel == null) {
                    _logger.LogError($"Hotel with {model.Id} not found, update failed");
                    return false;
                }
                result[result.IndexOf(hotel)] = model;
                _cache.Set("hotels", result);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }
        }
    }
}
