﻿namespace HotelSearch.Models.Dto
{
    public class Hotel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
    }
}
