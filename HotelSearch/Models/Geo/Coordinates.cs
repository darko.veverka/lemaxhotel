﻿namespace HotelSearch.Models.Geo
{
    public class Coordinates
    {
        public double Lat { get; set; }
        public double Lon { get; set; }
    }
}
