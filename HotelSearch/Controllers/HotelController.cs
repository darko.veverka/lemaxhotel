﻿using HotelSearch.Interface;
using HotelSearch.Models.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace HotelSearch.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class HotelController : Controller
    {
        private readonly ILogger<HotelController> _logger;
        private readonly IRepository _repository;

        public HotelController(ILogger<HotelController> logger, IRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpGet]
        [Produces("application/json")]
        [SwaggerOperation(Summary = "Returns all hotels")]
        public async Task<ActionResult<IList<Hotel>>> Get()
        {
            var result = await _repository.GetAllAsync();
            return Ok(result);
        }

        [HttpGet]
        [Route("{id}")]
        [Produces("application/json")]
        [SwaggerOperation(Summary = "Returns a Hotel by {id}")]
        public async Task<ActionResult<Hotel>> Get(string id, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _repository.GetAsync(id, cancellationToken);
                if (result == null)
                    return NotFound();

                return Ok(result);
            }
            catch (ArgumentException aex)
            {
                return Problem(statusCode: (int)HttpStatusCode.BadRequest, detail: aex.Message);
            }
        }

        [HttpPost]
        [Route("insert")]
        [Produces("application/json")]
        [SwaggerOperation(Summary = "Saves new hotel")]
        public async Task<IActionResult> Save([FromBody] Hotel model, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _repository.SaveAsync(model, cancellationToken);

                if (!result)
                    return Problem(statusCode: (int)HttpStatusCode.InternalServerError, detail: "Hotel was not saved");

                return Ok();
            }
            catch (ArgumentException aex)
            {
                return Problem(statusCode: (int)HttpStatusCode.BadRequest, detail: aex.Message);
            }
        }

        [HttpPatch]
        [Route("update")]
        [Produces("application/json")]
        [SwaggerOperation(Summary = "Updates a hotel")]
        public async Task<IActionResult> Update([FromBody] Hotel model, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _repository.UpdateAsync(model, cancellationToken);

                if (!result)
                    return Problem(statusCode: (int)HttpStatusCode.InternalServerError, detail: "Hotel was not updated");

                return Ok();
            }
            catch (ArgumentException aex)
            {
                return Problem(statusCode: (int)HttpStatusCode.BadRequest, detail: aex.Message);
            }
        }

        [HttpDelete]
        [Route("{id}")]
        [Produces("application/json")]
        [SwaggerOperation(Summary = "Deletes a hotel by {id}")]
        public async Task<IActionResult> Delete(string id, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _repository.DeleteAsync(id, cancellationToken);

                if (!result)
                    return Problem(statusCode: (int)HttpStatusCode.InternalServerError, detail: "Hotel does not exist or could not be deleted");

                return Ok();
            }
            catch (ArgumentException aex)
            {
                return Problem(statusCode: (int)HttpStatusCode.BadRequest, detail: aex.Message);
            }
        }
    }
}

