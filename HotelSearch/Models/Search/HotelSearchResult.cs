﻿namespace HotelSearch.Models.Search
{
    public class HotelSearchResult
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal Distance { get; set; }
    }
}
