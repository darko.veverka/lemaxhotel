﻿using HotelSearch.Models.Dto;

namespace HotelSearch.Interface
{
    public interface IRepository
    {
        Task<List<Hotel>> GetAllAsync(CancellationToken cancellationToken = default);
        Task<Hotel> GetAsync(string id, CancellationToken cancellationToken = default);
        Task<bool> SaveAsync(Hotel model, CancellationToken cancellationToken = default);
        Task<bool> UpdateAsync(Hotel model, CancellationToken cancellationToken = default);
        public Task<bool> DeleteAsync(string id, CancellationToken cancellationToken = default);
    }
}
